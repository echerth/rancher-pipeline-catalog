version: '2'
volumes:
  pgdata:
    driver: ${VOLUME_DRIVER}
    
services:
  postgres:
    image: postgres:${POSTGRES_TAG}
    environment:
      PGDATA: "/var/lib/postgresql/data/pgdata"
      POSTGRES_DB: "${postgres_db}"
      POSTGRES_USER: "${postgres_user}"
      POSTGRES_PASSWORD: "${postgres_password}"
    tty: true
    stdin_open: true
{{- if eq.Values.use_dns_name_check "true"}}
  labels:
   - "traefik.enable=true"
   - "traefik.domain=${domain_to_register_dns}"
   - "traefik.port=8081"
   - "traefik.alias.fqdn=${dns_name_value}"
   - "postgres"
{{- end}}

